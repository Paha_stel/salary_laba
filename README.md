# db prepare
Запустите SQL Server Manager, запустите createBD.sql для создания базы данных
# Credentials
В проекте по умолчанию создан один аккаунт
Login: admin
Password: admin
# Aplication prepare
Создайте файл connections.properties в корне приложения (папка app).
Заполните этот файл в соответствии с требованиями для подключения к вашей базе данныйх.
Пример:
```bash
Data Source=localhost
Initial Catalog=Test
Integrated Security=True
```