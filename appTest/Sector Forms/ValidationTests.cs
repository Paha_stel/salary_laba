﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using app.Sector_Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.Sector_Forms.Tests
{
    [TestClass()]
    public class ValidationTests
    {
        [TestMethod()]
        public void IsEmptyFieldTest_ShoulReturnTrue()
        {
            string field = "";

            Assert.IsTrue(Validation.IsEmptyField(field));
        }

        [TestMethod()]
        public void IsEmptyFieldTest_ShoulReturnFalse()
        {
            string field = "fill";

            Assert.IsFalse(Validation.IsEmptyField(field));
        }

        [TestMethod()]
        public void IsTrimmedEmptyFieldTest_ShoulReturnTrue()
        {
            string field = "   ";

            Assert.IsTrue(Validation.IsTrimmedEmptyField(field));
        }

        [TestMethod()]
        public void IsTrimmedEmptyFieldTest_ShoulReturnFalse()
        {
            string field = "   fill   ";

            Assert.IsFalse(Validation.IsTrimmedEmptyField(field));
        }
    }
}