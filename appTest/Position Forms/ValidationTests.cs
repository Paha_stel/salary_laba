﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using app.Position_Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.Position_Forms.Tests
{
    [TestClass()]
    public class ValidationTests
    {
        [TestMethod()]
        public void IsEmptyFieldTest_ShoulReturnTrue()
        {
            string field = "";

            Assert.IsTrue(Validation.IsEmptyField(field));
        }

        [TestMethod()]
        public void IsEmptyFieldTest_ShoulReturnFalse()
        {
            string field = "fill";

            Assert.IsFalse(Validation.IsEmptyField(field));
        }

        [TestMethod()]
        public void AreTwoTrimmedNotEmptyFieldsTest_FirstEmptySecondEmpty()
        {
            string firstFieldTrimmed = "";
            string secondFieldTrimmed = "";
            string firstField = "   ";
            string secondField = "   ";

            Assert.IsFalse(Validation.AreTwoTrimmedNotEmptyFields(firstFieldTrimmed, secondFieldTrimmed));
            Assert.IsFalse(Validation.AreTwoTrimmedNotEmptyFields(firstField, secondField));
        }

        [TestMethod()]
        public void AreTwoTrimmedNotEmptyFieldsTest_FirstFullSecondEmpty()
        {
            string firstFieldTrimmed = "fill";
            string secondFieldTrimmed = "";
            string firstField = "  fill ";
            string secondField = "   ";

            Assert.IsFalse(Validation.AreTwoTrimmedNotEmptyFields(firstFieldTrimmed, secondFieldTrimmed));
            Assert.IsFalse(Validation.AreTwoTrimmedNotEmptyFields(firstField, secondField));
        }

        [TestMethod()]
        public void AreTwoTrimmedNotEmptyFieldsTest_FirstEmptySecondFull()
        {
            string firstFieldTrimmed = "";
            string secondFieldTrimmed = "fill";
            string firstField = "   ";
            string secondField = "  fill ";

            Assert.IsFalse(Validation.AreTwoTrimmedNotEmptyFields(firstFieldTrimmed, secondFieldTrimmed));
            Assert.IsFalse(Validation.AreTwoTrimmedNotEmptyFields(firstField, secondField));
        }

        [TestMethod()]
        public void AreTwoTrimmedNotEmptyFieldsTest_FirstFullSecondFull()
        {
            string firstFieldTrimmed = "fill";
            string secondFieldTrimmed = "fill";
            string firstField = " fill  ";
            string secondField = " fill  ";

            Assert.IsTrue(Validation.AreTwoTrimmedNotEmptyFields(firstFieldTrimmed, secondFieldTrimmed));
            Assert.IsTrue(Validation.AreTwoTrimmedNotEmptyFields(firstField, secondField));
        }
    }
}