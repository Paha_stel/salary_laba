﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using app.dBConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.dBConnection.Tests
{
    [TestClass()]
    public class DBConnectionTests
    {

        [TestMethod()]
        public void parseFileTest()
        {
            string result = DBConnection.parseFile("../../mock.connection.properties.txt");
            string expectedValues = "key=value;";
        
            Assert.AreEqual(result, expectedValues);
        }
    }
}