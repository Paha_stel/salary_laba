﻿using app.dBConnection;
using app.models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace app.Position_Forms
{
    public partial class PositionAddForm : Form
    {
        private DataContext db = DBConnection.createConnectionFromFile("../../connection.properties");

        public PositionAddForm()
        {
            InitializeComponent();
        }

        private void textBoxPositionName_TextChanged(object sender, EventArgs e)
        {
            buttonAdd.Enabled = !Validation.IsEmptyField(textBoxPositionName.Text);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            var positionName = textBoxPositionName.Text;
            var positionAccess = textBox1.Text;

            if (Validation.AreTwoTrimmedNotEmptyFields(positionName, positionAccess))
            {
                var table = db.GetTable<Position>();

                Position position = new Position { name = textBoxPositionName.Text, accessLevel = int.Parse(positionAccess) };
                table.InsertOnSubmit(position);
                db.SubmitChanges();
                Close();
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
