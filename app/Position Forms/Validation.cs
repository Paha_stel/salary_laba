﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.Position_Forms
{
    public class Validation
    {
        public static bool IsEmptyField(string field)
        {
            return field == "";
        }

        public static bool AreTwoTrimmedNotEmptyFields(string firstField, string secondFileds)
        {
            return firstField.Trim() != "" && secondFileds.Trim() != "";
        }
    }
}
