﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace app.Position_Forms
{
    public partial class PositionEditForm : Form
    {
        public PositionEditForm()
        {
            InitializeComponent();
        }

        private void textBoxPositionName_TextChanged(object sender, EventArgs e)
        {
            buttonEdit.Enabled = textBoxPositionName.Text != "";
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
