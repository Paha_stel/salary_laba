﻿using app.dBConnection;
using app.models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace app.Sector_Forms
{
    public partial class SectorAddForm : Form
    {
        private DataContext db = DBConnection.createConnectionFromFile("../../connection.properties");

        public SectorAddForm()
        {
            InitializeComponent();
        }

        private void textBoxSectorName_TextChanged(object sender, EventArgs e)
        {
            buttonAdd.Enabled = !Validation.IsEmptyField(textBoxSectorName.Text);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            var adress = textBoxSectorName.Text;

            if (!Validation.IsTrimmedEmptyField(adress))
            {
                Area area = new Area { Address = adress };
                db.GetTable<Area>().InsertOnSubmit(area);
                db.SubmitChanges();
                Close();
            }
        }
    }
}
