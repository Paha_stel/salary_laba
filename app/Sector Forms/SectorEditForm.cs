﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace app.Sector_Forms
{
    public partial class SectorEditForm : Form
    {
        public SectorEditForm()
        {
            InitializeComponent();
        }

        private void textBoxSectorName_TextChanged(object sender, EventArgs e)
        {
            buttonEdit.Enabled = !Validation.IsEmptyField(textBoxSectorName.Text);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
