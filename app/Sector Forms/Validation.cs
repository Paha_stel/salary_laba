﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.Sector_Forms
{
    public class Validation
    {
        public static bool IsEmptyField(string field)
        {
            return field == "";
        }

        public static bool IsTrimmedEmptyField(string field)
        {
            return field.Trim() == "";
        }
    }
}
