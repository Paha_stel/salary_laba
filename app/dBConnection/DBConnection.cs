﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq;
using System.IO;

namespace app.dBConnection
{
    public class DBConnection
    {
        public static DataContext createConnectionFromFile(string filepath) {
            
            return new DataContext(parseFile(filepath));
        }

        public static string parseFile(string filepath) {
            var data = new Dictionary<string, string>();
            StringBuilder builder = new StringBuilder();
            foreach (var row in File.ReadAllLines(filepath))
                builder.Append(row.Split('=')[0] + "=" + row.Split('=')[1] + ";");

            return builder.ToString();
        }
    }
}
