﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace app.models
{

    [Table(Name = "WorkOnArea")]
    public class WorkOnArea
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id;
        [Column]
        public int AreaId;
        private EntityRef<Area> _Area;
        [Association(Storage = "_Area", ThisKey = "AreaId")]
        public Area Area
        {
            get { return this._Area.Entity; }
            set {
                this.AreaId = value.Id;
                this._Area.Entity = value;
            }
        }
        [Column]
        public int WorkerId;
        private EntityRef<Worker> _Worker;
        [Association(Storage = "_Worker", ThisKey = "WorkerId")]
        public Worker Worker
        {
            get { return this._Worker.Entity; }
            set {
                this.WorkerId = value.Id;
                this._Worker.Entity = value;
            }
        }

        [Column]
        public int AmountOfWork;

        [Column]
        public double productPrice;

        [Column(DbType = "date")]
        public DateTime startWork;

        [Column(DbType = "date")]
        public DateTime endWork;

        public WorkOnArea()
        {
        }

        public override bool Equals(object obj)
        {
            var area = obj as WorkOnArea;
            return area != null &&
                   Id == area.Id &&
                   AmountOfWork == area.AmountOfWork;
        }

        public override int GetHashCode()
        {
            var hashCode = 1355625897;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + AmountOfWork.GetHashCode();
            return hashCode;
        }
    }
}
