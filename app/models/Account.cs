﻿using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace app.models
{
    [Table(Name = "Account")]
    public class Account
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id;
        [Column]
        public string Login { get; set; }
        [Column]
        public string Password { get; set; }
        public int WorkerId { get; set; }
        private EntityRef<Worker> _Worker;
        [Association(Storage = "_Worker", ThisKey = "WorkerId")]
        public Worker Worker
        {
            get { return this._Worker.Entity; }
            set
            {
                this.WorkerId = value.Id;
                this._Worker.Entity = value;
            }
        }

        public Account()
        {
        }

        public override bool Equals(object obj)
        {
            var account = obj as Account;
            return account != null &&
                   Id == account.Id &&
                   Login == account.Login &&
                   Password == account.Password;
        }

        public override int GetHashCode()
        {
            var hashCode = 2079674330;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Login);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Password);
            return hashCode;
        }
    }
}
