﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.models
{
    [Table(Name = "Area")]
    public class Area
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { get; set; }
        [Column]
        public string Address { get; set; }

        public Area() {}

        public override bool Equals(object obj)
        {
            var area = obj as Area;
            return area != null &&
                   Id == area.Id &&
                   Address == area.Address;
        }

        public override int GetHashCode()
        {
            var hashCode = -306707981;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Address);
            return hashCode;
        }
    }
}
