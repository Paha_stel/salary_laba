﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.models
{
    [Table(Name = "Worker")]
    public class Worker
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id { get; set; }
        [Column]
        public string FIO { get; set; }
        [Column]
        public int PositionId { get; set; }
        private EntityRef<Position> _Position;
        [Association(Storage = "_Position", ThisKey = "PositionId")]
        public Position Position
        {
            get { return this._Position.Entity; }
            set {
                this.PositionId = value.id;
                this._Position.Entity = value;
            }
        }

        public Worker() {

        }

        public override bool Equals(object obj)
        {
            var worker = obj as Worker;
            return worker != null &&
                   Id == worker.Id &&
                   FIO == worker.FIO;
        }

        public override int GetHashCode()
        {
            var hashCode = -1972281651;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(FIO);
            return hashCode;
        }
    }
}
