﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.models
{
    [Table(Name = "TotalSalary")]
    public class TotalSalary
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int Id;
        [Column]
        public int WorkerId;
        private EntityRef<Worker> _Worker;
        [Association(Storage = "_Worker", ThisKey = "WorkerId")]
        public Worker Worker
        {
            get { return this._Worker.Entity; }
            set {
                this.WorkerId = value.Id;
                this._Worker.Entity = value;
            }
        }
        [Column]
        public int AmountOfWork;
        [Column]
        public int Salary;
        [Column]
        public DateTime Date;

        public TotalSalary()
        {
        }

        public override bool Equals(object obj)
        {
            var salary = obj as TotalSalary;
            return salary != null &&
                   Id == salary.Id &&
                   WorkerId == salary.WorkerId &&
                   Salary == salary.Salary;
        }

        public override int GetHashCode()
        {
            var hashCode = -1650006707;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + WorkerId.GetHashCode();
            hashCode = hashCode * -1521134295 + Salary.GetHashCode();
            return hashCode;
        }
    }
}
