﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app.models
{
    [Table(Name = "Position")]
    public class Position
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int id;
        [Column]
        public string name;
        [Column]
        public int accessLevel;

        public Position()
        {
        }

        public override bool Equals(object obj)
        {
            var position = obj as Position;
            return position != null &&
                   id == position.id &&
                   name == position.name &&
                   accessLevel == position.accessLevel;
        }

        public override int GetHashCode()
        {
            var hashCode = 233261025;
            hashCode = hashCode * -1521134295 + id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(name);
            hashCode = hashCode * -1521134295 + accessLevel.GetHashCode();
            return hashCode;
        }
    }
}
