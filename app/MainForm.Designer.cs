﻿namespace app
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.labelTable = new System.Windows.Forms.Label();
            this.buttonPrintEmployee = new System.Windows.Forms.Button();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.dataGridViewTable = new System.Windows.Forms.DataGridView();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonSavePDF = new System.Windows.Forms.Button();
            this.buttonShowsReports = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonShowPositions = new System.Windows.Forms.Button();
            this.buttonShowCompletedWork = new System.Windows.Forms.Button();
            this.buttonShowSalary = new System.Windows.Forms.Button();
            this.buttonShowSectors = new System.Windows.Forms.Button();
            this.panelButtons = new System.Windows.Forms.Panel();
            this.buttonUsers = new System.Windows.Forms.Button();
            this.buttonShowEmployees = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.textBoxSearch = new System.Windows.Forms.TextBox();
            this.buttonSearch = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxLogo.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxLogo.Image")));
            this.pictureBoxLogo.Location = new System.Drawing.Point(619, 303);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(410, 147);
            this.pictureBoxLogo.TabIndex = 47;
            this.pictureBoxLogo.TabStop = false;
            // 
            // labelTable
            // 
            this.labelTable.AutoSize = true;
            this.labelTable.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTable.ForeColor = System.Drawing.Color.White;
            this.labelTable.Location = new System.Drawing.Point(373, 13);
            this.labelTable.Name = "labelTable";
            this.labelTable.Size = new System.Drawing.Size(0, 26);
            this.labelTable.TabIndex = 46;
            // 
            // buttonPrintEmployee
            // 
            this.buttonPrintEmployee.FlatAppearance.BorderColor = System.Drawing.Color.SlateGray;
            this.buttonPrintEmployee.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPrintEmployee.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonPrintEmployee.ForeColor = System.Drawing.Color.Black;
            this.buttonPrintEmployee.Location = new System.Drawing.Point(942, 664);
            this.buttonPrintEmployee.Name = "buttonPrintEmployee";
            this.buttonPrintEmployee.Size = new System.Drawing.Size(150, 49);
            this.buttonPrintEmployee.TabIndex = 45;
            this.buttonPrintEmployee.Text = "Печать по сотруднику";
            this.buttonPrintEmployee.UseVisualStyleBackColor = true;
            this.buttonPrintEmployee.Visible = false;
            // 
            // buttonPrint
            // 
            this.buttonPrint.FlatAppearance.BorderColor = System.Drawing.Color.SlateGray;
            this.buttonPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPrint.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonPrint.ForeColor = System.Drawing.Color.Black;
            this.buttonPrint.Location = new System.Drawing.Point(771, 664);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(150, 49);
            this.buttonPrint.TabIndex = 44;
            this.buttonPrint.Text = "Печать";
            this.buttonPrint.UseVisualStyleBackColor = true;
            this.buttonPrint.Visible = false;
            // 
            // dataGridViewTable
            // 
            this.dataGridViewTable.AllowUserToAddRows = false;
            this.dataGridViewTable.AllowUserToDeleteRows = false;
            this.dataGridViewTable.BackgroundColor = System.Drawing.Color.LightGray;
            this.dataGridViewTable.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewTable.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.HotTrack;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewTable.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTable.EnableHeadersVisualStyles = false;
            this.dataGridViewTable.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridViewTable.Location = new System.Drawing.Point(356, 41);
            this.dataGridViewTable.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridViewTable.MultiSelect = false;
            this.dataGridViewTable.Name = "dataGridViewTable";
            this.dataGridViewTable.ReadOnly = true;
            this.dataGridViewTable.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTable.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewTable.RowHeadersVisible = false;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(91)))));
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.Padding = new System.Windows.Forms.Padding(5);
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.White;
            this.dataGridViewTable.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewTable.RowTemplate.Height = 35;
            this.dataGridViewTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewTable.Size = new System.Drawing.Size(905, 605);
            this.dataGridViewTable.TabIndex = 39;
            this.dataGridViewTable.Visible = false;
            // 
            // buttonEdit
            // 
            this.buttonEdit.FlatAppearance.BorderColor = System.Drawing.Color.SlateGray;
            this.buttonEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEdit.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonEdit.ForeColor = System.Drawing.Color.Black;
            this.buttonEdit.Location = new System.Drawing.Point(1149, 664);
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.Size = new System.Drawing.Size(112, 49);
            this.buttonEdit.TabIndex = 41;
            this.buttonEdit.Text = "Изменить";
            this.buttonEdit.UseVisualStyleBackColor = true;
            this.buttonEdit.Visible = false;
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.FlatAppearance.BorderColor = System.Drawing.Color.SlateGray;
            this.buttonAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdd.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAdd.ForeColor = System.Drawing.Color.Black;
            this.buttonAdd.Location = new System.Drawing.Point(995, 664);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(112, 49);
            this.buttonAdd.TabIndex = 40;
            this.buttonAdd.Text = "Добавить";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Visible = false;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonSavePDF
            // 
            this.buttonSavePDF.FlatAppearance.BorderColor = System.Drawing.Color.SlateGray;
            this.buttonSavePDF.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSavePDF.Font = new System.Drawing.Font("Calibri", 13.25F);
            this.buttonSavePDF.ForeColor = System.Drawing.Color.Black;
            this.buttonSavePDF.Location = new System.Drawing.Point(1111, 664);
            this.buttonSavePDF.Name = "buttonSavePDF";
            this.buttonSavePDF.Size = new System.Drawing.Size(150, 49);
            this.buttonSavePDF.TabIndex = 43;
            this.buttonSavePDF.Text = "Сохранить в PDF";
            this.buttonSavePDF.UseVisualStyleBackColor = true;
            this.buttonSavePDF.Visible = false;
            // 
            // buttonShowsReports
            // 
            this.buttonShowsReports.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonShowsReports.FlatAppearance.BorderSize = 0;
            this.buttonShowsReports.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(171)))), ((int)(((byte)(179)))));
            this.buttonShowsReports.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShowsReports.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonShowsReports.ForeColor = System.Drawing.Color.White;
            this.buttonShowsReports.Location = new System.Drawing.Point(0, 566);
            this.buttonShowsReports.Name = "buttonShowsReports";
            this.buttonShowsReports.Size = new System.Drawing.Size(295, 80);
            this.buttonShowsReports.TabIndex = 7;
            this.buttonShowsReports.Text = "Отчёты";
            this.buttonShowsReports.UseVisualStyleBackColor = true;
            this.buttonShowsReports.Click += new System.EventHandler(this.buttonShowsReports_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(15, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(265, 100);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // buttonShowPositions
            // 
            this.buttonShowPositions.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonShowPositions.FlatAppearance.BorderSize = 0;
            this.buttonShowPositions.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(171)))), ((int)(((byte)(179)))));
            this.buttonShowPositions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShowPositions.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonShowPositions.ForeColor = System.Drawing.Color.White;
            this.buttonShowPositions.Location = new System.Drawing.Point(0, 406);
            this.buttonShowPositions.Name = "buttonShowPositions";
            this.buttonShowPositions.Size = new System.Drawing.Size(295, 80);
            this.buttonShowPositions.TabIndex = 1;
            this.buttonShowPositions.Text = "Список всех должностей";
            this.buttonShowPositions.UseVisualStyleBackColor = true;
            this.buttonShowPositions.Click += new System.EventHandler(this.buttonShowPositions_Click);
            // 
            // buttonShowCompletedWork
            // 
            this.buttonShowCompletedWork.BackColor = System.Drawing.Color.SlateGray;
            this.buttonShowCompletedWork.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonShowCompletedWork.FlatAppearance.BorderSize = 0;
            this.buttonShowCompletedWork.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(171)))), ((int)(((byte)(179)))));
            this.buttonShowCompletedWork.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShowCompletedWork.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonShowCompletedWork.ForeColor = System.Drawing.Color.White;
            this.buttonShowCompletedWork.Location = new System.Drawing.Point(0, 166);
            this.buttonShowCompletedWork.Name = "buttonShowCompletedWork";
            this.buttonShowCompletedWork.Size = new System.Drawing.Size(295, 80);
            this.buttonShowCompletedWork.TabIndex = 3;
            this.buttonShowCompletedWork.Text = "Ведомости выполненной работы";
            this.buttonShowCompletedWork.UseVisualStyleBackColor = false;
            this.buttonShowCompletedWork.Click += new System.EventHandler(this.buttonShowCompletedWork_Click);
            // 
            // buttonShowSalary
            // 
            this.buttonShowSalary.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonShowSalary.FlatAppearance.BorderSize = 0;
            this.buttonShowSalary.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(171)))), ((int)(((byte)(179)))));
            this.buttonShowSalary.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShowSalary.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonShowSalary.ForeColor = System.Drawing.Color.White;
            this.buttonShowSalary.Location = new System.Drawing.Point(0, 246);
            this.buttonShowSalary.Name = "buttonShowSalary";
            this.buttonShowSalary.Size = new System.Drawing.Size(295, 80);
            this.buttonShowSalary.TabIndex = 4;
            this.buttonShowSalary.Text = "Ведомости начисления з/п";
            this.buttonShowSalary.UseVisualStyleBackColor = true;
            this.buttonShowSalary.Click += new System.EventHandler(this.buttonShowSalary_Click);
            // 
            // buttonShowSectors
            // 
            this.buttonShowSectors.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonShowSectors.FlatAppearance.BorderSize = 0;
            this.buttonShowSectors.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(171)))), ((int)(((byte)(179)))));
            this.buttonShowSectors.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShowSectors.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonShowSectors.ForeColor = System.Drawing.Color.White;
            this.buttonShowSectors.Location = new System.Drawing.Point(0, 326);
            this.buttonShowSectors.Name = "buttonShowSectors";
            this.buttonShowSectors.Size = new System.Drawing.Size(295, 80);
            this.buttonShowSectors.TabIndex = 2;
            this.buttonShowSectors.Text = "Список всех участков";
            this.buttonShowSectors.UseVisualStyleBackColor = true;
            this.buttonShowSectors.Click += new System.EventHandler(this.buttonShowSectors_Click);
            // 
            // panelButtons
            // 
            this.panelButtons.BackColor = System.Drawing.Color.SlateGray;
            this.panelButtons.Controls.Add(this.buttonUsers);
            this.panelButtons.Controls.Add(this.buttonShowsReports);
            this.panelButtons.Controls.Add(this.pictureBox1);
            this.panelButtons.Controls.Add(this.buttonShowEmployees);
            this.panelButtons.Controls.Add(this.buttonShowPositions);
            this.panelButtons.Controls.Add(this.buttonShowCompletedWork);
            this.panelButtons.Controls.Add(this.buttonShowSalary);
            this.panelButtons.Controls.Add(this.buttonShowSectors);
            this.panelButtons.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelButtons.Location = new System.Drawing.Point(0, 0);
            this.panelButtons.Name = "panelButtons";
            this.panelButtons.Size = new System.Drawing.Size(295, 756);
            this.panelButtons.TabIndex = 38;
            // 
            // buttonUsers
            // 
            this.buttonUsers.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonUsers.FlatAppearance.BorderSize = 0;
            this.buttonUsers.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(171)))), ((int)(((byte)(179)))));
            this.buttonUsers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUsers.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonUsers.ForeColor = System.Drawing.Color.White;
            this.buttonUsers.Location = new System.Drawing.Point(0, 646);
            this.buttonUsers.Name = "buttonUsers";
            this.buttonUsers.Size = new System.Drawing.Size(295, 80);
            this.buttonUsers.TabIndex = 8;
            this.buttonUsers.Text = "Панель управления пользователями";
            this.buttonUsers.UseVisualStyleBackColor = true;
            this.buttonUsers.Click += new System.EventHandler(this.buttonUsers_Click);
            // 
            // buttonShowEmployees
            // 
            this.buttonShowEmployees.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.buttonShowEmployees.FlatAppearance.BorderSize = 0;
            this.buttonShowEmployees.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(165)))), ((int)(((byte)(171)))), ((int)(((byte)(179)))));
            this.buttonShowEmployees.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShowEmployees.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonShowEmployees.ForeColor = System.Drawing.Color.White;
            this.buttonShowEmployees.Location = new System.Drawing.Point(0, 486);
            this.buttonShowEmployees.Name = "buttonShowEmployees";
            this.buttonShowEmployees.Size = new System.Drawing.Size(295, 80);
            this.buttonShowEmployees.TabIndex = 0;
            this.buttonShowEmployees.Text = "Список всех работников";
            this.buttonShowEmployees.UseVisualStyleBackColor = true;
            this.buttonShowEmployees.Click += new System.EventHandler(this.buttonShowEmployees_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.buttonDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDelete.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonDelete.ForeColor = System.Drawing.Color.Black;
            this.buttonDelete.Location = new System.Drawing.Point(841, 664);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(112, 49);
            this.buttonDelete.TabIndex = 48;
            this.buttonDelete.Text = "Удалить";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Visible = false;
            // 
            // textBoxSearch
            // 
            this.textBoxSearch.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxSearch.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxSearch.Location = new System.Drawing.Point(356, 664);
            this.textBoxSearch.Name = "textBoxSearch";
            this.textBoxSearch.Size = new System.Drawing.Size(233, 31);
            this.textBoxSearch.TabIndex = 49;
            this.textBoxSearch.Visible = false;
            // 
            // buttonSearch
            // 
            this.buttonSearch.FlatAppearance.BorderColor = System.Drawing.Color.SlateGray;
            this.buttonSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSearch.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonSearch.ForeColor = System.Drawing.Color.Black;
            this.buttonSearch.Location = new System.Drawing.Point(595, 664);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(108, 31);
            this.buttonSearch.TabIndex = 50;
            this.buttonSearch.Text = "Поиск";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Visible = false;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1309, 756);
            this.Controls.Add(this.buttonSearch);
            this.Controls.Add(this.textBoxSearch);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.pictureBoxLogo);
            this.Controls.Add(this.labelTable);
            this.Controls.Add(this.buttonPrintEmployee);
            this.Controls.Add(this.buttonPrint);
            this.Controls.Add(this.dataGridViewTable);
            this.Controls.Add(this.buttonEdit);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.buttonSavePDF);
            this.Controls.Add(this.panelButtons);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Главная";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelButtons.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxLogo;
        private System.Windows.Forms.Label labelTable;
        private System.Windows.Forms.Button buttonPrintEmployee;
        private System.Windows.Forms.Button buttonPrint;
        private System.Windows.Forms.DataGridView dataGridViewTable;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonSavePDF;
        private System.Windows.Forms.Button buttonShowsReports;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buttonShowPositions;
        private System.Windows.Forms.Button buttonShowCompletedWork;
        private System.Windows.Forms.Button buttonShowSalary;
        private System.Windows.Forms.Button buttonShowSectors;
        private System.Windows.Forms.Panel panelButtons;
        private System.Windows.Forms.Button buttonShowEmployees;
        private System.Windows.Forms.Button buttonUsers;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.TextBox textBoxSearch;
        private System.Windows.Forms.Button buttonSearch;
    }
}