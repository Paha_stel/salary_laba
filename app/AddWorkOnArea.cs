﻿using app.dBConnection;
using app.models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace app
{   
    public partial class AddWorkOnArea : Form
    {
        private DataContext db = DBConnection.createConnectionFromFile("../../connection.properties");
        private int index;

        public AddWorkOnArea(int index = -1)
        {
            this.index = index;
            InitializeComponent();

            List<Worker> workersList = db.GetTable<Worker>().ToList<Worker>();
            List<Area> areaList = db.GetTable<Area>().ToList<Area>();

            comboBox1.ValueMember = "Id";
            comboBox1.DisplayMember = "FIO";
            comboBox1.DataSource = workersList;

            comboBox2.ValueMember = "Id";
            comboBox2.DisplayMember = "Address";
            comboBox2.DataSource = areaList;

            if (index != -1)
            {
                var workOnArea = db.GetTable<WorkOnArea>().ToList()[index];
                comboBox1.SelectedValue = workOnArea.WorkerId;
                comboBox2.SelectedValue = workOnArea.AreaId;
                textBox1.Text = workOnArea.productPrice.ToString();
                dateTimePicker1.Value = workOnArea.startWork;
                dateTimePicker2.Value = workOnArea.endWork;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var workerId = int.Parse(comboBox1.SelectedValue.ToString());
            var areaId = int.Parse(comboBox2.SelectedValue.ToString());
            var productPrice = int.Parse(textBox1.Text);
            var startWork = dateTimePicker1.Value.Date;
            var endWork = dateTimePicker2.Value.Date;

            var table = db.GetTable<WorkOnArea>();
           
            if (index == -1)
            {
                WorkOnArea workOnArea = new WorkOnArea
                {
                    WorkerId = workerId,
                    AreaId = areaId,
                    productPrice = productPrice,
                    startWork = startWork,
                    endWork = endWork,
                };

                table.InsertOnSubmit(workOnArea);
            }
            else
            {
                var workOnArea = db.GetTable<WorkOnArea>().Skip(index).First();

                workOnArea.WorkerId = workerId;
                workOnArea.AreaId = areaId;
                workOnArea.productPrice = productPrice;
                workOnArea.startWork = startWork;
                workOnArea.endWork = endWork;
            }

            db.SubmitChanges();
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
