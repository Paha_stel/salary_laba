﻿using app.dBConnection;
using app.models;
using System;
using System.Data;
using System.Data.Linq;
using System.Linq;
using System.Windows.Forms;

namespace app
{
    public partial class LogInForm : Form
    {
        public LogInForm()
        {
            InitializeComponent();
        }

        private void buttonEnter_Click(object sender, EventArgs e)
        {
            Account account = null;
            using (DataContext db = DBConnection.createConnectionFromFile("../../connection.properties"))
            {
                account = (from u in db.GetTable<Account>()
                           where u.Login == textBoxLogin.Text && u.Password == textBoxPassword.Text
                           select u).FirstOrDefault();

                //if (temp.Count() != 0) {
                //    account = temp.First();
                //}
            }

            if (account != null)
            {
                Hide();
                MainForm mainForm = new MainForm();
                mainForm.Show();
            }
            else
            {
                labelError.Visible = true;
            }
        }
    }
}
