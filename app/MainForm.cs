﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Linq;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using app.dBConnection;
using app.Employee_Forms;
using app.models;
using app.Position_Forms;
using app.Sector_Forms;
using app.Users_Panel_Forms;

namespace app
{
    public partial class MainForm : Form
    {
        private int checker;
        private DataContext db = DBConnection.createConnectionFromFile("../../connection.properties");

        public MainForm()
        {
            InitializeComponent();
        }

        public void newConnection()
        {
            db = DBConnection.createConnectionFromFile("../../connection.properties");
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void buttonShowCompletedWork_Click(object sender, EventArgs e)
        {
            checker = 0;

            ResetBtnColor();
            buttonShowCompletedWork.BackColor = Color.FromArgb(180, 180, 180);

            buttonPrint.Visible = false;
            buttonPrintEmployee.Visible = false;
            buttonSavePDF.Visible = false;

            buttonAdd.Visible = true;
            buttonEdit.Visible = true;
            buttonDelete.Visible = false;

            dataGridViewTable.Visible = true;
            pictureBoxLogo.Visible = false;

            textBoxSearch.Visible = true;
            buttonSearch.Visible = true;

            ClearTable();

            newConnection();
            var query = db.GetTable<WorkOnArea>();

            var column1 = new DataGridViewTextBoxColumn();
            column1.HeaderText = "ФИО";
            column1.Name = "Column1";

            var column2 = new DataGridViewTextBoxColumn();
            column2.HeaderText = "Цена товара";
            column2.Name = "Column2";

            var column3 = new DataGridViewTextBoxColumn();
            column3.HeaderText = "Дата начала работы";
            column3.Name = "Column3";

            var column4 = new DataGridViewTextBoxColumn();
            column4.HeaderText = "Дата окончания работы";
            column4.Name = "Column4";

            var column5 = new DataGridViewTextBoxColumn();
            column5.HeaderText = "Участок";
            column5.Name = "Column5";

            dataGridViewTable.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewTable.Columns.AddRange(new DataGridViewColumn[] { column1, column2, column3, column4, column5 });

            foreach (var workOnArea in query)
            {
                var index = dataGridViewTable.Rows.Add();
                var lastRow = dataGridViewTable.Rows[index];

                lastRow.Cells[0].Value = workOnArea.Worker.FIO;
                lastRow.Cells[1].Value = workOnArea.productPrice;
                lastRow.Cells[2].Value = workOnArea.startWork;
                lastRow.Cells[3].Value = workOnArea.endWork;
                lastRow.Cells[4].Value = workOnArea.Area.Address;
            }
        }

        private void buttonShowSalary_Click(object sender, EventArgs e)
        {
            checker = 5;

            ResetBtnColor();
            buttonShowSalary.BackColor = Color.FromArgb(180, 180, 180);

            buttonPrint.Visible = true;
            buttonPrintEmployee.Visible = true;
            buttonSavePDF.Visible = true;

            buttonAdd.Visible = false;
            buttonEdit.Visible = false;
            buttonDelete.Visible = false;

            dataGridViewTable.Visible = true;
            pictureBoxLogo.Visible = false;

            textBoxSearch.Visible = true;
            buttonSearch.Visible = true;

            ClearTable();

            var query = db.GetTable<TotalSalary>();

            var column1 = new DataGridViewTextBoxColumn();
            column1.HeaderText = "ФИО";
            column1.Name = "Column1";

            var column2 = new DataGridViewTextBoxColumn();
            column2.HeaderText = "Зарплата";
            column2.Name = "Column2";

            var column3 = new DataGridViewTextBoxColumn();
            column3.HeaderText = "Дата";
            column3.Name = "Column3";

            dataGridViewTable.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewTable.Columns.AddRange(new DataGridViewColumn[] { column1, column2, column3 });

            foreach (var salary in query)
            {
                var index = dataGridViewTable.Rows.Add();
                var lastRow = dataGridViewTable.Rows[index];

                lastRow.Cells[0].Value = salary.Worker.FIO;
                lastRow.Cells[1].Value = salary.Salary;
                lastRow.Cells[2].Value = salary.Date.ToString();
            }
        }

        private void buttonShowSectors_Click(object sender, EventArgs e)
        {
            checker = 1;

            ResetBtnColor();
            buttonShowSectors.BackColor = Color.FromArgb(180, 180, 180);

            buttonPrint.Visible = false;
            buttonPrintEmployee.Visible = false;
            buttonSavePDF.Visible = false;

            buttonAdd.Visible = true;
            buttonEdit.Visible = true;
            buttonDelete.Visible = false;

            dataGridViewTable.Visible = true;
            pictureBoxLogo.Visible = false;

            textBoxSearch.Visible = false;
            buttonSearch.Visible = false;

            ClearTable();

            var query = db.GetTable<Area>();

            var column1 = new DataGridViewTextBoxColumn();
            column1.HeaderText = "Название участка";
            column1.Name = "Column1";
            dataGridViewTable.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewTable.Columns.AddRange(new DataGridViewColumn[] { column1 });

            foreach (var area in query)
            {
                var index = dataGridViewTable.Rows.Add();
                var lastRow = dataGridViewTable.Rows[index];

                lastRow.Cells[0].Value = area.Address;
            }
        }

        private void buttonShowPositions_Click(object sender, EventArgs e)
        {
            checker = 2;

            ResetBtnColor();
            buttonShowPositions.BackColor = Color.FromArgb(180, 180, 180);

            buttonPrint.Visible = false;
            buttonPrintEmployee.Visible = false;
            buttonSavePDF.Visible = false;

            buttonAdd.Visible = true;
            buttonEdit.Visible = true;
            buttonDelete.Visible = false;

            dataGridViewTable.Visible = true;
            pictureBoxLogo.Visible = false;

            textBoxSearch.Visible = false;
            buttonSearch.Visible = false;

            ClearTable();

            var query = db.GetTable<Position>();

            var column1 = new DataGridViewTextBoxColumn();
            column1.HeaderText = "Название должности";
            column1.Name = "Column1";

            var column2 = new DataGridViewTextBoxColumn();
            column2.HeaderText = "Уровень доступа";
            column2.Name = "Column2";

            dataGridViewTable.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewTable.Columns.AddRange(new DataGridViewColumn[] { column1, column2 });

            foreach (var position in query)
            {
                var index = dataGridViewTable.Rows.Add();
                var lastRow = dataGridViewTable.Rows[index];

                lastRow.Cells[0].Value = position.name;
                lastRow.Cells[1].Value = position.accessLevel.ToString();
            }
        }

        private void buttonShowEmployees_Click(object sender, EventArgs e)
        {
            checker = 3;

            ResetBtnColor();
            buttonShowEmployees.BackColor = Color.FromArgb(180, 180, 180);

            buttonPrint.Visible = false;
            buttonPrintEmployee.Visible = false;
            buttonSavePDF.Visible = false;

            buttonAdd.Visible = true;
            buttonEdit.Visible = true;
            buttonDelete.Visible = false;

            dataGridViewTable.Visible = true;
            pictureBoxLogo.Visible = false;

            textBoxSearch.Visible = true;
            buttonSearch.Visible = true;

            ClearTable();


            var query = db.GetTable<Worker>();

            var column1 = new DataGridViewTextBoxColumn();
            column1.HeaderText = "ФИО";
            column1.Name = "Column1";
            var column2 = new DataGridViewTextBoxColumn();
            column2.HeaderText = "Должность";
            column2.Name = "Column2";

            dataGridViewTable.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewTable.Columns.AddRange(new DataGridViewColumn[] { column1, column2 });

            foreach (var worker in query)
            {
                var index = dataGridViewTable.Rows.Add();
                var lastRow = dataGridViewTable.Rows[index];

                lastRow.Cells[0].Value = worker.FIO;
                lastRow.Cells[1].Value = worker.Position.name;
            }
        }

        private void buttonShowsReports_Click(object sender, EventArgs e)
        {
            ResetBtnColor();
            buttonShowsReports.BackColor = Color.FromArgb(180, 180, 180);

            buttonPrint.Visible = true;
            buttonPrintEmployee.Visible = true;
            buttonSavePDF.Visible = true;

            buttonAdd.Visible = false;
            buttonEdit.Visible = false;
            buttonDelete.Visible = false;

            dataGridViewTable.Visible = true;
            pictureBoxLogo.Visible = false;

            textBoxSearch.Visible = false;
            buttonSearch.Visible = false;
        }

        private void ResetBtnColor()
        {
            buttonShowEmployees.BackColor = Color.SlateGray;
            buttonShowCompletedWork.BackColor = Color.SlateGray;
            buttonShowPositions.BackColor = Color.SlateGray;
            buttonShowSalary.BackColor = Color.SlateGray;
            buttonShowSectors.BackColor = Color.SlateGray;
            buttonShowsReports.BackColor = Color.SlateGray;
            buttonUsers.BackColor = Color.SlateGray;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            ResetBtnColor();

            buttonPrint.Visible = false;
            buttonPrintEmployee.Visible = false;
            buttonSavePDF.Visible = false;

            buttonAdd.Visible = false;
            buttonEdit.Visible = false;
            buttonDelete.Visible = false;

            dataGridViewTable.Visible = false;
            pictureBoxLogo.Visible = true;

            textBoxSearch.Visible = false;
            buttonSearch.Visible = false;
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            switch (checker)
            {
                case 0:
                    AddWorkOnArea addWorkInAreaForm = new AddWorkOnArea();
                    addWorkInAreaForm.FormClosed += WorkOnAreaClosed;
                    addWorkInAreaForm.ShowDialog();
                    break;
                case 1:
                    SectorAddForm sectorAddForm = new SectorAddForm();
                    sectorAddForm.FormClosed += SectorFromClosed;
                    sectorAddForm.ShowDialog();
                    break;
                case 2:
                    PositionAddForm positionAddForm = new PositionAddForm();
                    positionAddForm.FormClosed += PositionFromClosed;
                    positionAddForm.ShowDialog();
                    break;
                case 3:
                    EmployeeAddForm employeeAddForm = new EmployeeAddForm();
                    employeeAddForm.ShowDialog();
                    break;
                case 4:
                    UserAddForm userAddForm = new UserAddForm();
                    userAddForm.ShowDialog();
                    break;
            }
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            switch (checker)
            {
                case 0:
                    var index = dataGridViewTable.SelectedRows[0].Index;
                    AddWorkOnArea addWorkInAreaForm = new AddWorkOnArea(index);
                    addWorkInAreaForm.FormClosed += WorkOnAreaClosed;
                    addWorkInAreaForm.ShowDialog();
                    break;
                case 1:
                    SectorEditForm sectorEditForm = new SectorEditForm();
                    sectorEditForm.ShowDialog();
                    break;
                case 2:
                    PositionEditForm positionEditForm = new PositionEditForm();
                    positionEditForm.ShowDialog();
                    break;
                case 3:
                    EmployeeEditForm employeeEditForm = new EmployeeEditForm();
                    employeeEditForm.ShowDialog();
                    break;
                case 4:
                    UserEditForm userEditForm = new UserEditForm();
                    userEditForm.ShowDialog();
                    break;
            }
        }

        private void ClearTable()
        {
            dataGridViewTable.Columns.Clear();
            dataGridViewTable.Rows.Clear();
        }

        private void SectorFromClosed(Object sender, FormClosedEventArgs e)
        {
            buttonShowSectors_Click(sender, e);
        }

        private void PositionFromClosed(Object sender, FormClosedEventArgs e)
        {
            buttonShowPositions_Click(sender, e);
        }

        private void WorkOnAreaClosed(Object sender, FormClosedEventArgs e)
        {
            buttonShowCompletedWork_Click(sender, e);
        }

        private void buttonUsers_Click(object sender, EventArgs e)
        {
            checker = 4;

            ResetBtnColor();
            buttonUsers.BackColor = Color.FromArgb(180, 180, 180);

            buttonPrint.Visible = false;
            buttonPrintEmployee.Visible = false;
            buttonSavePDF.Visible = false;

            buttonAdd.Visible = true;
            buttonEdit.Visible = true;
            buttonDelete.Visible = true;

            dataGridViewTable.Visible = true;
            pictureBoxLogo.Visible = false;

            textBoxSearch.Visible = false;
            buttonSearch.Visible = false;

            ClearTable();
        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            DataGridViewTextBoxColumn column1, column2, column3, column4, column5;

            switch (checker)
            {
                case 0:
                    ClearTable();

                    newConnection();
                    var queryWorkOnArea = db.GetTable<WorkOnArea>();

                    column1 = new DataGridViewTextBoxColumn();
                    column1.HeaderText = "ФИО";
                    column1.Name = "Column1";

                    column2 = new DataGridViewTextBoxColumn();
                    column2.HeaderText = "Цена товара";
                    column2.Name = "Column2";

                    column3 = new DataGridViewTextBoxColumn();
                    column3.HeaderText = "Дата начала работы";
                    column3.Name = "Column3";

                    column4 = new DataGridViewTextBoxColumn();
                    column4.HeaderText = "Дата окончания работы";
                    column4.Name = "Column4";

                    column5 = new DataGridViewTextBoxColumn();
                    column5.HeaderText = "Участок";
                    column5.Name = "Column5";

                    dataGridViewTable.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                    dataGridViewTable.Columns.AddRange(new DataGridViewColumn[] { column1, column2, column3, column4, column5 });

                    foreach (var workOnArea in queryWorkOnArea)
                    {
                        if (workOnArea.Worker.FIO.Contains(textBoxSearch.Text))
                        {
                            var index = dataGridViewTable.Rows.Add();
                            var lastRow = dataGridViewTable.Rows[index];

                            lastRow.Cells[0].Value = workOnArea.Worker.FIO;
                            lastRow.Cells[1].Value = workOnArea.productPrice;
                            lastRow.Cells[2].Value = workOnArea.startWork;
                            lastRow.Cells[3].Value = workOnArea.endWork;
                            lastRow.Cells[4].Value = workOnArea.Area.Address;
                        }
                    }
                    break;
                case 3:
                    ClearTable();

                    var queryWorker = db.GetTable<Worker>();

                    column1 = new DataGridViewTextBoxColumn();
                    column1.HeaderText = "ФИО";
                    column1.Name = "Column1";
                    column2 = new DataGridViewTextBoxColumn();
                    column2.HeaderText = "Должность";
                    column2.Name = "Column2";

                    dataGridViewTable.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                    dataGridViewTable.Columns.AddRange(new DataGridViewColumn[] { column1, column2 });

                    foreach (var worker in queryWorker)
                    {
                        if (worker.FIO.Contains(textBoxSearch.Text))
                        {
                            var index = dataGridViewTable.Rows.Add();
                            var lastRow = dataGridViewTable.Rows[index];

                            lastRow.Cells[0].Value = worker.FIO;
                            lastRow.Cells[1].Value = worker.Position.name;
                        }
                    }
                    break;
                case 5:
                    ClearTable();

                    var queryTotalSalary = db.GetTable<TotalSalary>();

                    column1 = new DataGridViewTextBoxColumn();
                    column1.HeaderText = "ФИО";
                    column1.Name = "Column1";

                    column2 = new DataGridViewTextBoxColumn();
                    column2.HeaderText = "Зарплата";
                    column2.Name = "Column2";

                    column3 = new DataGridViewTextBoxColumn();
                    column3.HeaderText = "Дата";
                    column3.Name = "Column3";

                    dataGridViewTable.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                    dataGridViewTable.Columns.AddRange(new DataGridViewColumn[] { column1, column2, column3 });

                    foreach (var salary in queryTotalSalary)
                    {
                        if (salary.Worker.FIO.Contains(textBoxSearch.Text))
                        {
                            var index = dataGridViewTable.Rows.Add();
                            var lastRow = dataGridViewTable.Rows[index];

                            lastRow.Cells[0].Value = salary.Worker.FIO;
                            lastRow.Cells[1].Value = salary.Salary;
                            lastRow.Cells[2].Value = salary.Date.ToString();
                        }
                    }
                    break;
            }
        }
    }
}
