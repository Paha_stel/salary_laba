DROP DATABASE IF EXISTS Test;
CREATE DATABASE Test;

go

use Test

CREATE table Area (
	Id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Address varchar(80) NOT NULL,
);

CREATE table Position (
	id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	name varchar(80) NOT NULL,
	accessLevel int NOT NULL
);

CREATE table Worker (
	id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	FIO varchar(80) NOT NULL,
	PositionId int NOT NULL,
	FOREIGN KEY (PositionId) REFERENCES Position (id)
);

CREATE table Account (
	Id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Login varchar(80) NOT NULL,
	Password varchar(80) NOT NULL,
	WorkerId int NOT NULL,
	FOREIGN KEY (WorkerId) REFERENCES Worker (id)
);

CREATE table WorkOnArea (
	Id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	AreaId int NOT NULL,
	WorkerId int NOT NULL,
	AmountOfWork int NOT NULL,
	productPrice float NOT NULL,
	startWork Date NOT NULL,
	endWork Date NOT NULL,
	FOREIGN KEY (AreaId) REFERENCES Area (Id),
	FOREIGN KEY (WorkerId) REFERENCES Worker (Id)
);

CREATE table TotalSalary (
	Id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	WorkerId int NOT NULL,
	AmountOfWork int NOT NULL,
	Salary int NOT NULL,
	Date Date NOT NULL,
	FOREIGN KEY (WorkerId) REFERENCES Worker (Id)
);

go

insert into Position (name, accessLevel)
values ('admin', 1);

go

insert into Worker (FIO, PositionId)
values ('����� ����� ���������', 1);

go

insert into Account (Login, Password, WorkerId)
values ('admin', 'admin', 1);
